/**
 *	PROJECT			:		CAMWP DEV
 *	DEVELOPER		:		CAMWP DEV Team
 *	DEVELOPER URI	:		https://camwpdev.com
 *	DATE			:		17-January-2018
 */
 
$( function() {

	var CREAM = {
		
		init: function() {
			
			this.loadBackgroundImage();
			this.sticky();
			this.toTop();
			this.smoothPageScroll();
			
		},
		
		loadBackgroundImage: function() {
			
			if ( $( '.has-bg' ).length ) {
				
				$( '.has-bg' ).each( function() {
				
					var bg = $( this ).attr( 'data-bg' );
					
					$( this ).css({
						
						'background' : 'url( ' + bg + ' ) no-repeat center center',
						
						'background-size' : 'cover'
						
					});
				
				});
				
			}
			
		},

		sticky: function() {

			$( window ).on( 'load resize scroll', function() {

				if ( $( window ).scrollTop() > $( '.header-fixed' ).height() ) {

					$( '.header-fixed' ).addClass( 'sticky' );

				} else {

					$( '.header-fixed' ).removeClass( 'sticky' );

				}

				$( '.header-fixed .navbar-toggler' ).attr( 'aria-expanded', false );
				$( '.header-fixed .navbar-collapse' ).removeClass( 'show' );

			});

		},

		toTop: function() {
			
			$( '.to-top' ).click( function( e ) {
				
				$( 'body, html' ).animate( { scrollTop: 0 }, 500 );
				
				e.preventDefault();
				
			});

		},

		smoothPageScroll: function() {
            
            var _that = this;
            
            var navbarHeight = $( '.navbar' ).height();
            
            $( 'a[href*="#"]:not([href="#"])' ).not( '.watch-video' ).click( function() {
                
                if ( location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname ) {
                    
                  var target = $( this.hash );
                  
                  target = target.length ? target : $( '[name=' + this.hash.slice(1) +']' );
                  
                  if ( target.length ) {
                      
                        $( 'html, body' ).animate({
                            
                            /* Make sure the part of the section is not hidden behind the fixed navbar */
							
                            scrollTop: target.offset().top - navbarHeight
                            
                        }, 500 );
                        
                        return false;
                    
                    }
                  
                }
            });
            
        }
		
	}
	
	$( document ).ready( function() {
		
		CREAM.init();
		
	});
	
});