jQuery( function( $ ) { 

	'use strict';

	var marker = [], infowindow = [], map;

	function addMarker( location, name, contentstr ) {

        marker[name] = new google.maps.Marker({
            position: location,
            map: map
        });

        marker[name].setMap( map );
		
		infowindow[name] = new google.maps.InfoWindow({
			content:contentstr
		});
		
		google.maps.event.addListener( marker[name], 'click', function() {
			infowindow[name].open( map,marker[name] );
		});

    }
	
	function initialize() {

		var lat = $( '#map-canvas' ).attr( "data-lat" );
		var lng = $( '#map-canvas' ).attr( "data-lng" );

		var myLatlng = new google.maps.LatLng( parseFloat( lat ), parseFloat( lng ) );

		var setZoom = parseInt( $( '#map-canvas' ).attr( "data-zoom" ) );

		var mapOptions = {
			zoom: setZoom,
            scrollwheel: false,
			panControl: false,
			panControlOptions: {
				position: google.maps.ControlPosition.LEFT_BOTTOM
			},
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.LARGE,
				position: google.maps.ControlPosition.LEFT_BOTTOM
			},
			streetViewControl: true,
			streetViewControlOptions: {
				position: google.maps.ControlPosition.LEFT_BOTTOM
			},
			
			center: myLatlng		
		};
		
		map = new google.maps.Map( document.getElementById( "map-canvas" ), mapOptions );		

		$( '.addresses-block a' ).each( function() {
			var mark_lat = $( this ).attr( 'data-lat' );
			var mark_lng = $( this ).attr( 'data-lng' );
			var this_index = $( '.addresses-block a' ).index( this );
			var mark_name = 'template_marker_' + this_index;
			var mark_locat = new google.maps.LatLng( mark_lat, mark_lng );
			var mark_str = $( this ).attr( 'data-string' );
			
			addMarker( mark_locat, mark_name, mark_str );	
		});
		
	}

	$( window ).ready( function() {
      	if ( $( '#map-canvas' ).length ) {
          	setTimeout( function() { initialize(); }, 500 ); 
      	}    
		
	});

});
